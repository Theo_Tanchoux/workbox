# TP Workbox

## Workbox

### Liste des fonctionnalités offertes

- Workbox window
  - Récupération des requêtes à la mise hors ligne de l'application pour pouvoir la restaurer plus tard
- Mise en cache des ressources
- Forcer la déconnexion d'un réseau trop lent pour s'adapter plus efficacement à l'environnement
- Accéder au cache depuis la window et non pas depuis le service worker
- Cache audio et vidéo
  - Augmentation des performances
- Gestion de la déconnexion réseau
- Alerte utilisateur de la mise à jour du service worker
- Scalabilité via plugins

### Méthodes de cache et utilisations possibles

- Cache uniquement
  - Mise en cache de tout les éléments de l'application aux premières requêtes éxécutées jusqu'à la prochaine mise à jour du service worker et sinon retour des éléments en cache
- Réseau uniquement
  - Requêtes à chaque fois, aucune mise en cache
- Cache en premier et réseau si non disponible
  1. Requête au cache, si l'élément existe il est retourné
  2. Si la requête n'est pas en cache, envoi réseau
  3. Quand la requête réseau est terminée, ajout de la requête au cache et retour de la requête réseau
- Réseau en premier et cache si non disponible
  1. Requête réseau en premier et mise en place de la requête en cache
  2. Si on passe hors-ligne, la dernière version du cache est retournée
- Caduque pendant la revalidation
  1. A la première requête, réseau en premier et mise en place de la requête en cache
  2. Pour les demandes ultérieures, retourner le cache en premier, puis en tâche de fond faire une requête réseau et mettre le cache à jour avec
  3. Pour les requêtes ensuite, vous recevrez la dernière version placée en cache via le réseau

### Uses cases à intégrer dans le projet doctoliberal

- La mise en cache des ressources
  - Réseau en premier et cache si non disponible
- La mise en place de la gestion réseau à la déconnexion
- Alerte utilisateur à la mise à jour du service worker
  - Prévenir les utilisateurs pro ou non d'une possible maintenance ou incohérence BDD dans la prise de RDV

---

## Page d'incitation à l'installation de PWA

### Créer un composant (React / Vue / angular / HTML+CSS) pour inciter et surtout guider un utilisateur à installer la PWA

```jsx
import React from "react";
import "./PWAComponent.css";

function PWAComponent() {
  return (
    <div className="pwa">
      <div className="pwa-container">
        <div className="title">PWA</div>
        <div className="installation">
          L'installation d'une PWA est très simple mais s'effectue différemment
          selon votre appareil
          <br />
          Vous pouvez l'installer sur Chrome sur Android en appuyant sur les
          trois points en haut à droite de l'écran et en appuyant sur ajouter à
          l'écran d'accueil
          <br />
          Sur iOS vous l'installez sur Safari en appuyant sur le bouton de
          partage en bas au milieu de l'écran et en appuyant sur "Sur l'écran
          d'accueil"
        </div>
        <div className="advantages">
          Notre PWA vous permettra d'avoir des fonctionnalités supplémentaires
          telles que la sauvegarde de l'application lorsque vous la quittez afin
          de pouvoir vous rapprocher le plus d'une application de votre appareil
          et ne pas perdre votre navigation, une simplicité d'accès sur votre
          écran d'accueil ainsi qu'une performance accrue
        </div>
      </div>
    </div>
  );
}

export default PWAComponent;
```

![image](pwa.png)

### Expliquer en quoi il serait interessant de mettre en place une telle page

L'utilisation d'une telle page pourrait amener plus de personnes à découvrir le monde des PWA, prévenir des éventuels manque de compatibilité afin de ne pas être mis en cause de bugs et guider l'utilisateur pour montrer qu'il est notre intérêt premier.
